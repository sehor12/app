import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginGroup: FormGroup;

  private url="http://localhost/proyecto/login.php";

  public dataRate: any=null;

  constructor(private loginBuilder:FormBuilder, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.buildForm();
  }


  private buildForm(){
    this.loginGroup=this.loginBuilder.group({
    user: ['',[Validators.required,Validators.maxLength(25)]],
    pass:['', [Validators.required, Validators.minLength(5)]],
    })
  }

  public Data(){
    const urlApi= this.url;
    const datos={
      "user": this.loginGroup.value.user,
      "pass": this.loginGroup.value.pass,
    };

    console.log(datos);
    this.httpClient.post(urlApi, datos)
      .subscribe(apiData => (this.dataRate=apiData));
    console.log(JSON.stringify(this.dataRate));
  }
}
