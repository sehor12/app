import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormularioComponent } from './formulario/formulario.component';
import { UserComponent } from './user.component';

const routes: Routes = [
  { path:'', component: UserComponent},
  { path: 'formulario', component: FormularioComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
